
const User = require("./../models/User");
const Course = require('./../models/Product');
const bcrypt = require('bcrypt');
const auth = require('./../auth');


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){ 
			return true
		} else {
			return false
		}
	})
}

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
} 


module.exports.login = (reqBody) => { 

	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


module.exports.getProfile = (data) => {
	console.log(data)

	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}

module.exports.customer = async (data) => {


	const userSaveStatus = await User.findById(data.userId).then( user => {

		user.customer.push({productId: data.productId})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


	const productSaveStatus = await Product.findById(data.productId).then( product => {
		product.customer.push({userId: data.userId})

		return product.save().then( (product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


	if(userSaveStatus && productSaveStatus){
		return true
	} else {
		return false
	}
}