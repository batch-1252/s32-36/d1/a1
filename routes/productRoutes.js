const express = require('express');
const router = express.Router();
let auth = require('./../auth');

const productController = require('./../controllers/productControllers');


router.get('/active', (req, res) => {
	productController.getAllActive().then( result => res.send(result));
})


router.get("/all", auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}
	productController.getAllProduct().then( result => res.send(result))
})



router.post('/addProduct', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}

	productController.addProduct(req.body).then( result => res.send(result))
})



router.get('/:productId', auth.verify, (req, res) => {

	productController.getSingleProduct(req.params).then( result => res.send(result))
})


router.put('/edit/:productId', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}

	productController.editProduct(req.params.productId, req.body).then( result => res.send(result))
})


router.put('/archive/:productId', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}

	productController.archiveProduct(req.params.productId).then( result => res.send(result))
})


router.put('/unarchive/:productId', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}

	productController.unarchiveProduct(req.params.productId).then( result => res.send(result))
})


router.delete('/delete/:productId', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}	

	productController.deleteProduct(req.params.productId).then( result => res.send(result))
})

module.exports = router;
